require './lib/random_trader/buy'
require './lib/random_trader/sell'

class RandomTrader
  attr_accessor :funds

  def initialize(funds)
    self.funds = funds.to_f
  end

  def perform
    public_send(%i[buy sell hold].sample)
  end

  def buy
    Buy.new(funds).perform
  end

  def sell
    Sell.new.perform
  end

  def hold
    puts 'Hold for today!'
  end
end
