require 'net/ftp'

class RandomTrader
  class Buy
    attr_accessor :funds

    def initialize(funds)
      self.funds = funds
    end

    def perform
      retrieve_data_file!
      puts "Spend $#{budget} on #{ticker}"
    end

    def budget
      rand(0..funds).ceil(2)
    end

    def ticker
      tickers.sample.split('|').first
    end

    protected

    def retrieve_data_file!
      Net::FTP.new('ftp.nasdaqtrader.com').tap do |ftp|
        ftp.login
        ftp.chdir('symboldirectory')
        ftp.gettextfile('nasdaqlisted.txt')
      end.close
    end

    def tickers
      @tickers ||= File.read('nasdaqlisted.txt').each_line.to_a[1..-2]
    end
  end
end
