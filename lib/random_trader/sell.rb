class RandomTrader
  class Sell
    def perform
      puts "Sell all shares of #{ticker}"
    end

    def ticker
      tickers.sample.split('|').first
    end

    protected

    def tickers
      @tickers ||= File.read('holdings.txt').each_line.to_a
    end
  end
end
