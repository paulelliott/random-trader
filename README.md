# Random Trader

That's right. It's suggests completely random trades. I had the idea for this
when I was reading Thinking Fast and Slow. There's a chapter where they talk
about working with a trading firm and while reading it I got to thinking about
how a completely random trading app would fare againt the market itself. Here's
how it works.

## Disclaimer

This is NOT intended to advise trades or provide you with any financial tips.
It is meant to aid a simulation of random market trades for educational or
informational purposes. Any trades you make are of your own volition and
neither I nor this app are liable for anything you may incur from such misuse.

## Process

This is designed to be run once a day on the command line.

1. Execute `rake random_trader[1234.56]` where `1234.56` is the total
   amount of funds available for buys.
2. It will randomly make a decision to either buy, sell, or hold.
3. On a buy it will randomly choose a ticker symbol from the nasdaq and
   randomly choose some number of available dollars to spend on that symbol.
4. On a sell it will randomly choose a ticker symbol you have to sell.
5. On a hold you should do nothing and try again tomorrow.

It relies on a data file with your current holdings. One symbol per line in
`holdings.txt`.
